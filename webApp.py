#!/usr/bin/env python3
import socket

port = 1234
class webApp:
    def process(self, ): # procesa la peticion
        print("Process: Return 200 OK")
        return "200 OK", "<html><body><h1>Hi!</h1></body></html>"

    def parse(self, request):
        parsed_request = request.split(" ")[1]
        return parsed_request

    def __init__(self, hostname, port):
        self.hostname = hostname
        self.port = port

        #creamos los sockets
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))

        mySocket.listen(5)
        try:
            while True:
                print("Waiting for connections")
                (recvSocket, address) = mySocket.accept()
                print(f"Connections of  {address} has been established succesfully.")
                request = recvSocket.recv(2048).decode('utf-8')
                parsed_request = self.parse(request)
                (Code, htmlAnswer) = self.process(parsed_request)
                response = "HTTP/1.1 " + Code + " \r\n\r\n" \
                           + htmlAnswer + "\r\n"
                recvSocket.send(response.encode('utf8'))
                recvSocket.close()

        except KeyboardInterrupt:
            print("Closing the server")
            recvSocket.close()
if __name__ == "__main__":
    only_webapp= webApp("localhost", port)
