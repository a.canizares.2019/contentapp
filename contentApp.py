#!/usr/bin/env python3

import webApp as wa
port = 1234
PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {body}
  </body>
</html>
"""

class ContentApp(wa.webApp):
    def __init__(self, hostname, port):
        self.contents = {'/': "<p>Main page: you can write /hello /bye or /2010</p>",
                         '/hello': "<p>Hello, welcome to this page</p>",
                         '/bye': "<p>Bye see you soon </p>",
                         '/2010': "<p>Best World cup ever </p>"}
        #heredamos del padre
        super().__init__(hostname, port)

    def process(self, resource):
        #miramos si esta la el recurso solicitado si se encuentra 200, si no 404
        if resource in self.contents:
            content = self.contents[resource]
            html_page = PAGE.format(body=content)
            code = "200 OK"
        else:
            code = "404 Not Found"
            html_page = "<html><body><h1>404 Not Found</h1></body></html>"
        return code, html_page


if __name__== '__main__':
    print("Listening port", port)
    cont_app = ContentApp("localhost", port)